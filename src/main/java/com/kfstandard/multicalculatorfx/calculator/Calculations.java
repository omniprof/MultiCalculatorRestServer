package com.kfstandard.multicalculatorfx.calculator;

import com.kfstandard.multicalculatorfx.data.FinanceBean;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/calc")
public class Calculations {

    /**
     * The Loan calculation
     *
     * @param money
     * @return 
     * @throws ArithmeticException
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/loan")
    public FinanceBean loanCalculation(FinanceBean money) throws ArithmeticException {

        // Divide APR by 12
        BigDecimal monthlyRate = money.getRate().divide(new BigDecimal("12"), MathContext.DECIMAL64);

        // At each step this variable is updated
        BigDecimal temp;
        // (1+rate)
        temp = BigDecimal.ONE.add(monthlyRate);

        // (1+rate)^term
        temp = temp.pow(money.getTerm().intValueExact());

        // BigDecimal pow does not support negative exponents so divide 1 by the result
        temp = BigDecimal.ONE.divide(temp, MathContext.DECIMAL64);

        // 1 - (1+rate)^-term
        temp = BigDecimal.ONE.subtract(temp);

        // rate / (1 - (1+rate)^-term)
        temp = monthlyRate.divide(temp, MathContext.DECIMAL64);

        // pv * (rate / 1 - (1+rate)^-term)
        temp = money.getInputValue().multiply(temp);

        // Round to 2 decimal places using banker's rounding
        temp = temp.setScale(2, RoundingMode.HALF_EVEN);

        // Remove the sign if the result is negative
        money.setResult(temp.abs());
        
        return money;
    }

    /**
     * The Future Value calculation
     *
     * @param money
     * @return 
     * @throws ArithmeticException
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/future")
    public FinanceBean futureValueCalculation(FinanceBean money) throws ArithmeticException {

        // Divide APR by 12
        BigDecimal monthlyRate = money.getRate().divide(new BigDecimal("12"), MathContext.DECIMAL64);

        // At each step this variable is updated
        BigDecimal temp;
        // (1+rate)
        temp = BigDecimal.ONE.add(monthlyRate);

        // (1+rate)^term
        temp = temp.pow(money.getTerm().intValueExact());

        // 1 - (1+rate)^-term
        temp = BigDecimal.ONE.subtract(temp);

        // (1 - (1+rate)^-term) / rate
        temp = temp.divide(monthlyRate, MathContext.DECIMAL64);

        // pv * (rate / 1 - (1+rate)^-term)
        temp = money.getInputValue().multiply(temp);

        // Round to 2 decimal places using banker's rounding
        temp = temp.setScale(2, RoundingMode.HALF_EVEN);

        // Remove the sign if the result is negative
        money.setResult(temp.abs());
        
        return money;

    }

    /**
     * The Savings Goal calculation
     *
     * @param money
     * @return 
     * @throws ArithmeticException
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/savings")
    public FinanceBean savingsGoalCalculation(FinanceBean money) throws ArithmeticException {

        // Divide APR by 12
        BigDecimal monthlyRate = money.getRate().divide(new BigDecimal("12"), MathContext.DECIMAL64);

        BigDecimal temp;

        // (1+rate)
        temp = BigDecimal.ONE.add(monthlyRate);

        // (1+rate)^term
        temp = temp.pow(money.getTerm().intValueExact());

        // 1 - ((1+rate)^term)
        temp = BigDecimal.ONE.subtract(temp);

        // rate / (1 - (1+rate)^term)
        temp = monthlyRate.divide(temp, MathContext.DECIMAL64);

        // fv * (rate / (1 - (1+rate)^term))
        temp = money.getInputValue().multiply(temp);

        temp = temp.setScale(2, RoundingMode.HALF_EVEN);

        // Remove the sign if the result is negative
        money.setResult(temp.abs());
        
        return money;
    }

}
